FROM node:alpine
WORKDIR /front
ARG CID
ARG CS
ENV CLIENT_ID=${CID}
ENV CLIENT_SECRET=${CS}
COPY package.json .
RUN npm install
COPY . .
RUN npm run build
CMD NITRO_SSL_CERT=$(cat local.crt) NITRO_SSL_KEY=$(cat local.key) CLIENT_ID=$CLIENT_ID CLIENT_SECRET=$CLIENT_SECRET node .output/server/index.mjs