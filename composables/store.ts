import { AccessTokenResponse, Items, SeveralArtists } from 'assets/interfaces/api.interface';
import { MyState } from 'assets/interfaces/store.interface';
import { RuntimeConfig } from 'nuxt/schema';
import  { defineStore } from 'pinia';

export const useMyStore = defineStore('main', {
  state: (): MyState => ({
    spotify_api_access_token: undefined,
    is_token_loaded: false,

    latest_artists: undefined,
    are_latest_artists_loaded: false,
  }),
  actions: {

    //permet de stocker le token d'accès dans le store
    async fetchSpotifyAPIAccessToken(): Promise<void> {

      const config: RuntimeConfig = useRuntimeConfig();
      const CLIENT_ID: string = config.CLIENT_ID;
      const CLIENT_SECRET: string = config.CLIENT_SECRET;

      const { data: response } = await useFetch<AccessTokenResponse>('https://accounts.spotify.com/api/token', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: `grant_type=client_credentials&client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}`
      });

      this.spotify_api_access_token = response.value?.access_token;
      this.is_token_loaded = true;
    },

    //permet de récupèrer les 5 derniers artistes recherchés et de les stocker dans le store
    async fetchLatestArtists(ids: string): Promise<void> {

      let get_several_artists_url: string = 'https://api.spotify.com/v1/artists?ids=' + ids;

      const { data: response } = await useFetch<SeveralArtists>(get_several_artists_url, 
        { 
          method: 'GET', headers: { 
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${this.spotify_api_access_token!}`
          } 
        }
      );

      this.latest_artists = response.value?.artists.reverse();
      this.are_latest_artists_loaded = true;
    },
  

  },
})