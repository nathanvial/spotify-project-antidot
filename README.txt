Les variables d'environnments pour se connecter à l'API de Spotify se nomment :

    CLIENT_ID
    CLIENT_SECRET

---------------------------------------------------------------------------------------

Lancer l'application avec HTTP : 

    1 - Installer les dépendances : 
        
        npm install

    2 - Lancer l'application en précisant les credentials spotify :

        CLIENT_ID=<client_id> CLIENT_SECRET=<client_secret> npm run dev

L'application est désormais accessible et fonctionnelle à l'adresse :

    http://localhost:3000

---------------------------------------------------------------------------------------

Lancer l'application avec HTTPS : 

    Avec DOCKER :

        1 - Installer les dépendances :

            npm install

        2 - Ajouter dans votre navigateur Chromium l'autorisation du fichier 'monCA.pem'.
            Pour cela suivre : Paramètres -> Sécurité -> Gérer les certificats -> Autorités -> Importer.
            Choisir ensuite le fichier 'monCA.pem' et cocher toutes les cases

        3 - Build l'image Docker à partir du Dockerfile :

            docker build --build-arg CID=<client_id> --build-arg CS=<client_secret> . --tag myapp

        4 - Lancer un conteneur bind sur le port 3000 : 

            docker run -p 3000:3000 -it myapp

    Sans DOCKER : 

        1 - Installer les dépendances : 

            npm install

        2 - Ajouter dans votre navigateur Chromium l'autorisation du fichier 'monCA.pem'.
            Pour cela suivre : Paramètres -> Sécurité -> Gérer les certificats -> Autorités -> Importer.
            Choisir ensuite le fichier 'monCA.pem' et cocher toutes les cases.
        
        3 - Exécuter la commande : 

            npm run securedev

L'application est désormais accessible et fonctionnelle à l'adresse :

    https://localhost:3000

----------------------------------------------------------------------------------------------

Quelques précisions :

    - Je n'ai finalement pas utilisé de back-end car je n'en voyais pas l'utilité dans ce projet (pas d'appel DB, sécurité des credentials spotify déjà assuré).
    - J'ai utilisé quelques images que j'ai laissé volontairement dans le dossier 'assets/images', car on reste en développement, j'aurais utilisé un dépôt en ligne sinon.
    - J'ai respecté tout ce que l'objectif demandait.
    - Je stocke les 5 derniers artistes recherchés dans les cookies utilisateur.


