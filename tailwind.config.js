/** @type {import('tailwindcss').Config} */
export const content = [];
export const theme = {
  colors: {
    'my-gray-1': '#131313',
    'my-gray-2': '#282828',
    'my-gray-3': '#181818',
    'my-green-1': '#83c878',
    'white': '#ffffff',
    'black': '#000000',
    transparent: 'transparent'
  },
  fontFamily: {
    kumbh: ['Kumbh Sans', 'sans-serif'],
  }
};
export const plugins = [];

