//toutes les interfaces liées aux objets de retour des fetch à l'API spotify

export interface AccessTokenResponse {
    access_token: string,
    expires_in: number
    token_type: string
}

export interface SearchArtistResponse {
    artists: Artists
}

interface Artists {
    href: string,
    items: Array<Items>,
    limit: number,
    next: string | null,
    offset: number,
    previous: string | null,
    total: number
}

export interface SeveralArtists {
    artists: Array<Items>
}

export interface Items {
    external_urls: {
        spotify: string
    },
    followers: {
        href: string | null,
        total: number
    },
    genres: Array<string>,
    href: string,
    id: string,
    images: Array<Image> | [],
    name: string,
    popularity: number,
    type: string,
    uri: string
}

interface Image {
    height: number,
    url?: string,
    width: number
}

export interface Albums {
    href: string,
    items: Array<AlbumsItems>,
    limit: number,
    next: string | null,
    offset: number,
    previous: string | null,
    total: number
}

export interface AlbumsItems{
    album_group?: string,
    album_type: string,
    artists: Array<AlbumsArtist>,
    available_markets: Array<string>,
    external_urls: {
        spotify: string,
    }
    href: string,
    id: string,
    images: Array<Image>,
    name: string,
    release_date: string,
    release_date_precision: string,
    total_tracks: number,
    type: string,
    uri: string,
}

interface AlbumsArtist{
    external_urls: {
        spotify: string,
    }
    href: string,
    id: string,
    name: string,
    type: string,
    uri: string,
}

export interface AlbumTracksResponse {
    href: string,
    items: Array<AlbumTracksItem>,
    limit: number,
    next: string | null,
    offset: number,
    previous: string | null,
    total: number
}

interface AlbumTracksItem {
    artists: Array<AlbumsArtist>,
    available_markets: Array<string>,
    disc_number: number,
    duration_ms: number,
    explicit: boolean,
    external_urls: {
        spotify: string,
    },
    href: string,
    id: string,
    is_local: boolean,
    name: string,
    preview_url: string,
    track_number: number,
    type: string,
    uri: string,
}

export interface SeveralTracks {
    tracks: Array<Track>
}

export interface Track {
    album: AlbumsItems,
    artists: Array<AlbumsArtist>,
    available_markets: Array<string>,
    disc_number: number,
    duration_ms: number,
    explicit: boolean,
    external_ids: {
        isrc: string,
    },
    external_urls: {
        spotify: string,
    }
    href: string,
    id: string,
    is_local: boolean,
    name: string,
    popularity: number,
    preview_url: string,
    track_number: number,
    type: string,
    uri: string,
}