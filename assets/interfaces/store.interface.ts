//les interfaces pour le store

import { Items } from "./api.interface";

export interface MyState {
    spotify_api_access_token: string | undefined,
    is_token_loaded: boolean,
    latest_artists: Array<Items> | undefined,
    are_latest_artists_loaded: boolean,
}

export interface MyActions {
    fetchSpotifyAPIAccessToken(): Promise<void>,
    fetchLatestArtists(ids: string): Promise<void>,
}