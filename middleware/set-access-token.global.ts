import { CookieRef, defineNuxtRouteMiddleware, navigateTo, useRuntimeConfig } from "nuxt/app";
import { Store } from "pinia";
import { useMyStore } from '../composables/store';
import { MyState, MyActions } from '../assets/interfaces/store.interface';

export default defineNuxtRouteMiddleware(async (_, __) => {

    //définition du store et récupération du cookie 'artists' dans lequel on stocke les 5 derniers artistes recherchés
    const store: Store<"main", MyState, {}, MyActions> = useMyStore();
    const cookie: CookieRef<string | null | undefined> = useCookie('artists');

    //dans le cas où le token n'est pas encore chargé (lors de l'arrivé sur l'app) on le fetch
    if (!store.is_token_loaded) {
        const _: void = await store.fetchSpotifyAPIAccessToken();
    }

    //dans le cas où les 5 derniers artistes ne sont pas encore chargés (lors de l'arrivé sur l'app) on les récupère dans le cookie 'artists'
    if (!store.are_latest_artists_loaded) {
        if (cookie.value === undefined || cookie.value === null) {
            store.are_latest_artists_loaded = true;
        } else {
            const ids: string = cookie.value.split('%').join(',');
            const _: void = await store.fetchLatestArtists(cookie.value);
        }
    }

})